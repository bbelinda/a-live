import React from 'react'
import MainConponent from '../components/main'
import AliveContext from '../components/context'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currentState: 'users'
    }
  }

  updateCurrentState(state) {
    this.setState({
      currentState: state
    })
  }

  render() {
    let { currentState } = this.state

    return (
    <AliveContext.Provider value={{
      currentState: currentState,
      updateCurrentState: this.updateCurrentState.bind(this)
    }}>
      <MainConponent/>
    </AliveContext.Provider>
    )
  }
}

export default App
