import React from 'react'
import { Switch, Route } from 'react-router-dom'
import HeaderComponent from '../components/header'
import UserComponent from '../components/user'
import AdminTestComponent from '../components/user/admin'
import SideMenu from '../components/side_menu'
import ThemeComponent from '../components/theme'

const Content = () => (
  <div className="d-flex">
    <SideMenu />
    <div className="container-content">
      <HeaderComponent></HeaderComponent>
      <div className="content-body">
        <Switch>
          <Route path='/users' component={UserComponent}></Route>
          <Route path='/admin' component={AdminTestComponent}></Route>
          <Route path='/theme' component={ThemeComponent}></Route>
        </Switch>
      </div>
    </div>
  </div>
)

export default Content
