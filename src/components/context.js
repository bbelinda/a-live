import React from 'react'
const AliveContext = React.createContext({
  updateCurrentState: () => {}
});

export { AliveContext as default }
