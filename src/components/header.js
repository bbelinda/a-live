import React from 'react'
import Context from './context'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'


class HeaderComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowDropdownMenu: false
    }
  }
  getCurrentState(state) {
    switch(state) {
      case "users":
        return "All Users"

      case "admin":
        return "All Admins"

      default:
        return ""
    }
  }

  isToggleDropdownMenu() {
    this.setState({
      isShowDropdownMenu: !this.state.isShowDropdownMenu
    })
  }

  render() {
    return (
      <Context.Consumer>
        {
          ({currentState}) => {
            return (
              <nav className="navbar px-4 py-4" role="navigation" aria-label="dropdown navigation">
                <div className="d-flex align-items-center">
                  <h1 className="text-primary mb-0">
                    { this.getCurrentState(currentState)}
                  </h1>
                  <div className="dropdown icon ml-3">
                    <div onClick={() => this.isToggleDropdownMenu()}>
                        <FontAwesomeIcon icon={faCaretDown} />
                      </div>
                    <div className={`dropdown-menu ${this.state.isShowDropdownMenu ? 'show' : ''}`} aria-labelledby="dropdownMenuButton">
                      <span className="title pl-2 text-size-xxs">Groups</span>
                      <a className="dropdown-item" href="#">
                        Action
                      </a>
                      <a className="dropdown-item" href="#">Another action</a>
                      <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
              </nav>
            )
          }
        }
      </Context.Consumer>
    )
  }
}

export default HeaderComponent
