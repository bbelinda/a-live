import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Theme extends React.Component {
  render() {
    return (
      <div className="p-4">
        <div className="d-flex">
          <h2 className="mr-5">Bold</h2>
          <h2 className="text-semi-bold mr-5">SemiBold</h2>
          <h2 className="text-medium mr-5">Medium</h2>
          <h2 className="text-prompt-light">Light</h2>
        </div>
        <div>
          <h1>Heading</h1>
          <h2>Heading</h2>
          <h3>Heading</h3>
          <h4>Heading</h4>
        </div>

        <div>
          <input className="form-control mb-2" placeholder="Search" />
          <input className="form-control error mb-4" placeholder="Search" />
          <label className="checkbox-label mr-2">
            <input type="checkbox" />
            <span className="checkbox-custom"></span>
          </label>
          checkbox

          <div></div>
          <label className="radio-label mr-2">
            <input type="radio" />
            <span className="radio-custom"></span>
          </label>
          Radio
        </div>

        <div className="mt-5"></div>
        <div>
          <div className="btn-group" role="group" aria-label="Basic example">
            <button type="button" className="btn active">Left</button>
            <button type="button" className="btn">Middle</button>
            <button type="button" className="btn">Right</button>
          </div>
        </div>

        <div className="mt-3">
          <button type="button" className="btn btn-primary mr-2">Primary</button>
          <button type="button" className="btn btn-secondary mr-2">Secondary</button>
          <button type="button" className="btn btn-success mr-2">Success</button>
          <button type="button" className="btn btn-danger mr-2">Danger</button>
          <button type="button" className="btn btn-warning mr-2">Warning</button>
          <button type="button" className="btn btn-info mr-2">Info</button>
          <button type="button" className="btn btn-light mr-2">Light</button>
          <button type="button" className="btn btn-dark mr-2">Dark</button>
          <button type="button" className="btn btn-primary disabled mr-2">Primary</button>
          <button type="button" className="btn btn-secondary disabled mr-2">Secondary</button>
          <button type="button" className="btn btn-outline mr-2">Outline</button>
        </div>
        <div className="mt-3">
          <button type="button" className="btn btn-primary btn-sm mr-2">Small button</button>
          <button type="button" className="btn btn-secondary btn-sm">Small button</button>
        </div>

        <div className="mt-3">
          <button type="button" className="btn btn-primary mr-2">
          <i className="fas fa-plus"></i> Primary
          </button>
        </div>

        <div className="mt-5"></div>
        <div className="alert alert-success" role="alert">
          This is a success alert—check it out!
        </div>
        <div className="alert alert-danger" role="alert">
          This is a danger alert—check it out!
        </div>

        <div>
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-outline" data-dismiss="modal">Keep Edit</button>
                <button type="button" className="btn btn-primary">Discard</button>
              </div>
            </div>
          </div>

          <div className="modal-dialog danger" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-outline" data-dismiss="modal">Cancel</button>
                <button type="button" className="btn btn-danger">Delete</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Theme
