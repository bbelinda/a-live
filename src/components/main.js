import React from 'react'
import LoginPage from '../components/login'
import Content from '../routes/contents'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

class MainConponent extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={LoginPage}></Route>
          <Route component={Content} />
        </Switch>
      </Router>
    )
  }
}

export default MainConponent
