import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft, faUserLock, faUserFriends, faClock, faEllipsisV, faGlobe } from '@fortawesome/free-solid-svg-icons'
// import { farFaStroopwafel } from '@fortawesome/pro-solid-svg-icons'
import logoMenu from '../styles/images/logo-a-live.png'
import {
  Link, NavLink
} from "react-router-dom";
import Context from './context'


class SideMenu extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isCollapsedMenu: false,
      isShowDropdownMenu: false
    }
  }

  toggleSideMenu() {
    this.setState({
      isCollapsedMenu: !this.state.isCollapsedMenu
    })
  }

  displayIconToggle() {
    let getTemplate = <div className="p-3 d-flex align-items-center justify-content-between">
      <img src={logoMenu} width="134" /> <FontAwesomeIcon icon={faAngleLeft} className="text-teal-30" />
    </div>
    if(this.state.isCollapsedMenu) {
      getTemplate = "icon"
    }

    return (
      <a onClick={() => this.toggleSideMenu()}>
        { getTemplate }
      </a>
    )
  }

  isToggleDropdownMenu() {
    this.setState({
      isShowDropdownMenu: !this.state.isShowDropdownMenu
    })
  }

  render() {
    return (
      <Context.Consumer>
        {
          ({updateCurrentState}) =>
            <div className={`side-menu ${this.state.isCollapsedMenu ? 'collapsed': ''}`}>
              { this.displayIconToggle() }

              <div className={`mx-3 mb-3 p-2 time-box align-items-baseline ${this.state.isCollapsedMenu ? 'd-none': 'd-flex'}`}>
                <FontAwesomeIcon icon={faClock} className="text-teal-30 mr-2 mt-1" />
                <div className="text-teal-30 text-medium text-size-xs">
                  <div>Fri 14 Feb, 2019</div>
                  <div className="text-teal-20 text-size-sm">
                    21:30:05 <span className="text-teal-30 text-prompt-light">(GMT +7)</span>
                  </div>
                </div>
              </div>

              <div className="header-mini mb-2">
                Login as
              </div>
              <div className={`${this.state.isCollapsedMenu ? 'd-none': 'd-flex'} align-items-center justify-content-between m-3 text-teal-30`}>
                {/* <img src={logoMenu} /> */}
                <div>
                  <div>hello@alive.co</div>
                  <div className="text-size-xs text-medium">Admin</div>
                </div>
                <div className="dropdown ml-3">
                  <div onClick={() => this.isToggleDropdownMenu()}>
                    <FontAwesomeIcon icon={faEllipsisV} />
                  </div>
                  <div className={`dropdown-menu ${this.state.isShowDropdownMenu ? 'show' : ''}`} aria-labelledby="dropdownMenuButton">
                    <div className="px-3">
                      <span className="d-flex align-items-start">
                        <FontAwesomeIcon icon={faGlobe} className="pr-2" />
                        Language
                      </span>
                      <div className="btn-group" role="group" aria-label="Basic example">
                        <button type="button" className="btn">EN</button>
                        <button type="button" className="btn">ไทย</button>
                      </div>
                    </div>

                    <a className="dropdown-item" href="#">Logout</a>
                  </div>
                </div>
              </div>
              <div className="mb-4"></div>

              <ul className="list-menu px-0">
                <li className="menu active">
                  <NavLink to="/users" className="py-2"
                          activeClassName="active"
                          onClick={() => updateCurrentState('users')}>
                            <FontAwesomeIcon icon={faUserFriends} /> Users
                  </NavLink>
                </li>
                <li className="header-mini py-2">Groups</li>
                <li className="menu">
                  <NavLink to="/aa" className="py-2"
                          activeClassName="active"
                          onClick={() => updateCurrentState('admin')}>
                            <FontAwesomeIcon className="mr-1" icon={faUserFriends} /> Group A
                  </NavLink>
                </li>
                <li className="menu">
                  <NavLink to="/vv" className="py-2"
                          activeClassName="active"
                          onClick={() => updateCurrentState('admin')}>
                            <FontAwesomeIcon className="mr-1" icon={faUserFriends} /> Group B
                  </NavLink>
                </li>
                <li className="menu">
                  <NavLink to="/bb" className="py-2"
                          activeClassName="active"
                          onClick={() => updateCurrentState('admin')}>
                            <FontAwesomeIcon className="mr-1" icon={faUserFriends} /> Group C
                  </NavLink>
                </li>
                <li className="header-mini py-2">Manage</li>
                <li className="menu">
                  <NavLink to="/admin" className="py-2"
                          activeClassName="active"
                          onClick={() => updateCurrentState('admin')}>
                            <FontAwesomeIcon className="mr-1" icon={faUserLock} /> Admin
                  </NavLink>
                </li>
              </ul>


            </div>
        }
      </Context.Consumer>
    )
  }
}

export default SideMenu
