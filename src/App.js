// import React from 'react';
// import logo from './logo.svg';
// import './App.css';
// // import App from './routes/index';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link
// } from "react-router-dom";

// class App extends React.Component {
//   render() {
//     return (
//       <React.Fragment>
//         <Router>
//           <div>
//             <nav>
//               <ul>
//                 <li>
//                   <Link to="/">Home</Link>
//                 </li>
//                 <li>
//                   <Link to="/about">About</Link>
//                 </li>
//                 <li>
//                   <Link to="/users">Users</Link>
//                 </li>
//               </ul>
//             </nav>

//             {/* A <Switch> looks through its children <Route>s and
//                 renders the first one that matches the current URL. */}
//             <Switch>
//               <Route path="/about">
//                 <div>about</div>
//               </Route>
//               <Route path="/users">
//                 <div>user</div>
//               </Route>
//               <Route path="/">
//                 <div className="App">
//                   <header className="App-header">
//                     <img src={logo} className="App-logo" alt="logo" />
//                     <p>
//                       Edit <code>src/App.js</code> and save to reload.
//                     </p>
//                     <a
//                       className="App-link"
//                       href="https://reactjs.org"
//                       target="_blank"
//                       rel="noopener noreferrer"
//                     >
//                       Learn React aaa
//                     </a>
//                   </header>
//                 </div>
//               </Route>
//             </Switch>
//           </div>
//         </Router>
//       </React.Fragment>
//     );
//   }
// }
// // function App() {
// //   return (
// //     <div className="App">
// //       <header className="App-header">
// //         <img src={logo} className="App-logo" alt="logo" />
// //         <p>
// //           Edit <code>src/App.js</code> and save to reload.
// //         </p>
// //         <a
// //           className="App-link"
// //           href="https://reactjs.org"
// //           target="_blank"
// //           rel="noopener noreferrer"
// //         >
// //           Learn React
// //         </a>
// //       </header>
// //     </div>
// //   );
// // }

// export default App;
